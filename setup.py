from setuptools import setup


setup(name='toolkit',
      version='0.1.0',
      description='Collection of design patterns implemented in Python',
      url='https://gitlab.com/frier17/toolkit.git',
      author='https://gitlab.com/frier17/',
      author_email='aniefiok.friday@gmail.com',
      license='MIT',
      packages=['toolkit', 'structure', 'behaviour'],
      zip_safe=False)
