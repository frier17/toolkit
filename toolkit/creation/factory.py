from abc import ABC, abstractmethod
from weakref import WeakValueDictionary
from typing import Sequence
import copy
import re
import keyword
from typing import Mapping, Any


class AbstractFactory(ABC):

    @classmethod
    @abstractmethod
    def produce(cls, product, specification):
        pass


class Factory(AbstractFactory):
    registry = dict()
    cache = WeakValueDictionary()
    
    def _check_name(name: str, length: int = 50) -> bool:
        word = re.compile(r'[aA-zZ0-9_]')
        if not (isinstance(name, str) and word.match(name) and len(name) > length):
            raise RuntimeError(
                'Invalid name provided. Name should be alphanumeric '
                'and less than 50 characters')
        if name in keyword.kwlist:
            raise RuntimeError('Invalid name. Name should not be a Python reserved '
                               'keyword')
       return True if name else False

    @classmethod
    def register(cls, product: Any, definition: Any = None) -> None:
        if _check_name(product) and product not in cls.registry:
            members = None
            if isinstance(definition, Mapping):
                members = list(definition.keys())
            cls.registry.update({product: members})
            cls.cache.update({product: type(product, (object,), dict(definition))})

    @classmethod
    def produce(cls, product: Any, specification: Mapping = None, bases: Sequence = None) ->\
            Any:

        def _create(object_, bases_, dict_):
            try:
                prototype = None
                if not _check_name(product):
                    return
                if not bases_:
                    if dict_ and isinstance(dict_, Mapping):
                        prototype = type(object_, (object,), dict(dict_))
                    else:
                        prototype = type(object_, (object,), {})
                elif bases_ and isinstance(bases_, Sequence):
                    parents = tuple(
                        (x for x in bases_ if isinstance(x, str) or issubclass(x, type) or isinstance(x, object)))
                    if isinstance(dict_, Mapping):
                        prototype = type(product, parents, dict(dict_))
                    else:
                        prototype = type(product, parents, {})
                return prototype
            except TypeError:
                raise
            except Exception:
                raise

        if product in cls.registry:
            instance = cls.cache.get(product)
            if instance:
                return copy.deepcopy(instance)
        else:
            return _create(product, bases, specification)
