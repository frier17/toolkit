from abc import ABC, abstractmethod
from datetime import datetime
from enum import IntFlag, Enum
from functools import wraps
from typing import Dict, Any, NewType, Sequence, Mapping


__all__ = ('BaseEvent', 'BasePublisher', 'DefaultPublisher', 'BaseObserver', 'observe',
           'EventType')


class BaseEvent:
    """
    Models an event class for sending signal across application.
    Each signal is identified via constant name or value identified as enum
    """
    __slots__ = '_name', '_cancelable', '_source', '_timestamp', \
                '_payload', '_identity', '_listeners'

    def __init__(self, identity: Enum = None, name: str = None,
                 cancelable: bool = False, source: Any = None,
                 timestamp: float = None, payload: Dict = None) -> None:
        self._listeners = set()

        if isinstance(name, str) and len(name) < 50 and name.isalnum():
            self._name = name
        if isinstance(identity, Enum):
            self._identity = identity
        else:
            raise RuntimeError(
                'Event object should have unique _identity of Enum type')
        if cancelable:
            self._cancelable = bool(cancelable)
        if isinstance(source, BasePublisher):
            self._source = source
        self._timestamp = datetime.now().timestamp()
        if isinstance(payload, Mapping):
            self._payload = dict(payload)
        else:
            self._payload = {}
        if not source:
            self._source = self.__class__
        if isinstance(timestamp, float):
            self._timestamp = timestamp
        elif isinstance(timestamp, datetime):
            self._timestamp = timestamp.timestamp()

    def notify(self) -> None:
        for listener in self._listeners:
            listener.update(event=self)

    def attach(self, listener) -> None:
        if not isinstance(listener, BaseObserver):
            raise RuntimeError('Invalid event listener object provided. '
                               'Expected Observer but got %s' % type(listener))
        if listener not in self._listeners:
            self._listeners.add(listener)

    @property
    def name(self):
        return self._name

    @property
    def cancelable(self):
        return self._cancelable

    @cancelable.setter
    def cancelable(self, value):
        self._cancelable = bool(value)

    @property
    def source(self):
        return self._source

    @property
    def timestamp(self):
        return self._timestamp

    @property
    def payload(self):
        return self._payload

    @property
    def identity(self):
        return self._identity


class BasePublisher(ABC):
    """
    Defines an object which can publish a set of events that
    can be consumed by other objects
    """

    @classmethod
    @abstractmethod
    def publish(cls, event: BaseEvent = None) -> None:
        """
        Publish the registered event upon satisfaction of
        listed conditions (calls BaseEvent.notify())
        """
        pass

    @classmethod
    @abstractmethod
    def register(cls, event: BaseEvent, conditions: Any = None) -> None:
        """
        Add an event to the list of possible events
        which can be published by this object
        """
        pass

    @abstractmethod
    def disable_event(self, event: BaseEvent, conditions: Any = None) -> None:
        """
        Disable an event when set conditions are reached.
        Muted events are only implemented for given object
        and not across all instances of the class.
        Attempts to set BaseEvent.cancelable to True
        """
        pass

    @abstractmethod
    def enable_event(self, event: BaseEvent, conditions: Any = None) -> None:
        """
        Enable an event when set conditions are reached.
        Muted events are only implemented for given object
        and not across all instances of the class.
        """
        pass


# Sample application implementation
class EventFlag(IntFlag):
    STATE_CHANGE = 2
    DOCUMENT_ADDED = 3
    DOCUMENT_DELETED = 5
    NEW_USER_REGISTERED = 7
    BUS = 1


EventType = NewType('EventType', BaseEvent)


class BaseObserver(ABC):

    @abstractmethod
    def update(self, event: EventType = None, source: Any = None, state=None) -> None:
        pass


@wraps
def observe(event: BaseEvent, source: Any = None):
    # function/method wrapper for function to handle event by source object
    # registers the wrapped function into BusinessDocumentWatcher
    return NotImplemented


class DefaultPublisher(BasePublisher):
    _registry = dict()

    @classmethod
    def notify_all(cls):
        for conditions, signal in cls._registry.values():
            if conditions.satisfied():
                signal.notify()

    @classmethod
    def publish(cls, event: EventType = None) -> None:
        """
        Publish the registered event upon
        satisfaction of listed conditions (calls BaseEvent.notify())
        """
        if isinstance(event, BaseEvent):
            event.notify()
        elif isinstance(event, Sequence):
            signals = set([signal for signal in event if isinstance(signal, BaseEvent)])
            for signal in signals:
                signal.notify()

    @classmethod
    def register(cls, event: BaseEvent, conditions: Any = None):
        """
        Add an event to the list of possible events which
        can be published by this object
        """
        if not isinstance(event, BaseEvent):
            raise RuntimeError('Invalid object provided. '
                               'Expected Event but got %s' % type(event))
        if not callable(conditions):
            cls._registry[event.identity] = (conditions, event)

    def disable_event(self, event: BaseEvent, conditions: Any = None):
        """
        Disable an event when set conditions are reached.
        Muted events are only implemented for given object
        and not across all instances of the class.
        Attempts to set BaseEvent.cancelable to True
        """
        if isinstance(conditions, Sequence):
            satisfied = all([condition for condition in
                             conditions if condition.satisfied()])
            if satisfied:
                event.cancelable = True
                event.payload.update({'muted': True})
                self._registry[event.identity] = (conditions, event)

    def enable_event(self, event, conditions=None):
        """
        Enable an event when set conditions are reached.
        Muted events are only implemented for given object
        and not across all instances of the class.
        """
        if isinstance(conditions, Sequence):
            satisfied = all([condition for condition in conditions if condition.satisfied()])
            if satisfied:
                event.cancelable = False
                event.payload.update({'muted': False})
                self._registry[event.identity] = (conditions, event)
